// ==UserScript==
// @name        reuters rss redirector
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-reuters-rss-redirector
// @downloadURL https://bitbucket.org/dsjkvf/userscript-reuters-rss-redirector/raw/master/rrr.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-reuters-rss-redirector/raw/master/rrr.user.js
// @include     https://*.reuters.com/*
// @run-at      document-start
// @version     1.01
// @grant       none
// ==/UserScript==

if (/feedType=RSS/.test(window.location) && window.location.search) {
    var newLocation = window.location.protocol + '//'
                + window.location.host
                + window.location.pathname;
    window.location.replace(newLocation);
}
