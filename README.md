userscript-reuters-rss-redirector
=================================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or a similar extension, which -- upon opening a Reuters article from an RSS feed -- will strip the Urchin Tracking Module (`?utm`) from the URL.
